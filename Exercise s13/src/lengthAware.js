export const lengthAwareMixin = {
	computed: {
		reversed() {
			return this.text.split("").reverse().join("");
		},
		calculateLength() {
			return this.text + ' (' + this.text.length + ')';
		}
	}
}
