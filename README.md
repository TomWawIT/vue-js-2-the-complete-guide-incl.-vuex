# My solutions for 'Vue JS 2 - The Complete Guide (incl. Vuex)' course

Course content:

Section: 1 - Getting Started

Section: 2 - Using VueJS to Interact with the DOM

Section: 3 - Using Conditionals and Rendering Lists

Section: 4 - First Course Project - The Monster Slayer

Section: 5 - Understanding the VueJS Instance

Section: 6 - Moving to a "Real" Development Workflow with Webpack and Vue 
CLI

Section: 7 - An Introduction to Components

Section: 8 - Communicating between Components

Section: 9 - Advanced Component Usage

Section: 10 - Second Course Project - Wonderful Quotes

Section: 11 - Handling User Input with Forms

Section: 12 - Using and Creating Directives

Section: 13 - Improving your App with Filters and Mixins

Section: 14 - Adding Animations and Transitions

Section: 15 - Connecting to Servers via Http - Using vue-resource

Section: 16 - Routing in a VueJS Application