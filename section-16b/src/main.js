import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
	routes,
	mode: 'history',
	scrollBehavior(to, from, savedPosition) {
		// scroll to saved position (if set)
		if(savedPosition) {
			return savedPosition;
		}
		// scroll to hash (if was set)
		if(to.hash) {
			return { selector: to.hash };
		}
		// always scroll to this position: (top of the page in this case)
		//   return {x: 0, y: 0};
	}
});


router.beforeEach((top, from, next) => {
	console.log('Global beforeEach');
	next(); // allow user to continue
	// next(false); // abort operation
	// next('/'); // redirect
});

new Vue({
	el: '#app',
	router,
	render: h => h(App)
})
