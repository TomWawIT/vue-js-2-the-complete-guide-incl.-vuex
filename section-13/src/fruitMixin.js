// Mixin for local use:
export const fruitMixin = {
	data() {
		return{
			friuts: ['Apple', 'Banana', 'Mango', 'Melon'],
			filterText: ''
		}
	},
	computed: {
		filteredFriuts() {
			return this.friuts.filter((element) => {
				return element.match(this.filterText);
			});
		}
	},
	created() {
		console.log('Created!');
	}
}
